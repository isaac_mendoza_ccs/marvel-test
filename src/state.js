import React, {createContext, useContext, useReducer} from 'react';

export const initialState = {
  search: null,
  characterList: [],
  theme: 'defaultTheme',
  ComicsByCharacters: {},
  favoriteCharacters: {...JSON.parse( localStorage.getItem("favoriteCharacters") )}
};

export let themeCurrent = initialState.theme

export const reducer = (state, action) => {
  switch (action.type) {
    case 'search':
      return { ...state, search: action.value };
    case 'theme':
      themeCurrent = action.value;
      return { ...state, theme: action.value };
    case 'characterList':
      return { ...state, characterList: action.value };
    case 'addCharacterList':
      return { ...state, characterList: [...state.characterList , ...action.value] };
    case 'addComicByCharacter':
      return { ...state, ComicsByCharacters: {...state.ComicsByCharacters , ...action.value} };
    case 'favoriteCharacters':
      localStorage.setItem("favoriteCharacters", JSON.stringify(action.value) )
      return { ...state, favoriteCharacters: action.value };
    default:
      return state;
  }
};

export const StateContext = createContext();
export const StateProvider = ({reducer, initialState, children}) =>(
  <StateContext.Provider value={useReducer(reducer, initialState)}>
    {children}
  </StateContext.Provider>
);
export const useStateValue = () => useContext(StateContext);