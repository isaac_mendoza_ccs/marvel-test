import React from 'react';
import { ThemeProvider } from "styled-components";

const theme = {
        defaultTheme: {
            background: '#ffffff'
        },
        darkTheme: {
            background: '#000000'
        }

};

const Theme = ({ children }) => (
    <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default Theme;