import React from 'react';

import Theme from "./Theme";
import Main from './Containers/Main';
import Header from './Containers/Header';
import { AppContainer } from './Components/StylesComponents';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { StateProvider, initialState, reducer, themeCurrent } from './state';

const App = () => {

  return (
    <StateProvider initialState={initialState} reducer={reducer}>
      <Theme>
        <BrowserRouter>
          <AppContainer themeCurrent={themeCurrent}>
            <Switch>
              <Route exact path="/favorites">
                <Header />
                <Main />
              </Route>
              <Route path="/">
                <Header />
                <Main />
              </Route>
            </Switch>
          </AppContainer>
        </BrowserRouter>
      </Theme>
    </StateProvider>
  );  
};
export default App;

