import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles( theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    circularProgress: {
        position: 'fixed',
        top: 'calc(50% - 50px)',
        left: 'calc(50% - 50px)',
        '& svg': {
            width: '100px',
            height: '100px',
        }
    }
}));
export default useStyles;