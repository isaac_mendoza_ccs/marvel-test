import style from 'styled-components';

export const AppContainer = style.div`
background: ${({ theme, themeCurrent }) => theme[themeCurrent].background };
height: 100vh;
`

export const Header = style.div`
display: flex;
`;

export const Logo = style.div`
flex-grow: 1;
margin: 15px;
cursor: pointer;
border-right: 3px solid #e8e8e8;
`
export const InputSearch = style.div`
margin: 15px;
flex-grow: 20;
display: flex;
align-items: center;
& > svg {
    width: 40px;
    opacity: .3;
    height: 40px;
    cursor: pointer;
}
& > input {
    width: 100%;
    border: none;
    margin: 0 15px
}
`
export const Favorites = style.div`
flex-grow: 1;
margin: 15px;
display: flex;
align-items: center;
& > span {
    color: #a8a8a8;
    font-size: 30px;
    cursor: pointer;
    border-right: 3px solid #e8e8e8;
};
transition: all .3s;
&:hover {
    transform: scale(1.2)
}
`
export const MainContainer = style.div`
    padding: 40px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
`
export const TargetCharacter = style.div`
    margin: 15px;
    cursor: pointer;
    max-width: 300px;
    min-width: 300px;
    overflow: hidden;
    max-height: 300px;
    position: relative;
    border-radius: 5px;
    & > img {
        width: 100%
    }
`
export const LabelCharacterName = style.span`
    left: 0;
    padding: 15px;
    color: #ffffff;
    position: absolute;
    top: calc(100% - 50px);
    width: calc(100% - 30px);
    background: linear-gradient(0deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0.5032387955182073) 25%, rgba(0,0,0,0.7497373949579832) 50%, rgba(0,0,0,0.500437675070028) 75%, rgba(0,0,0,0) 100%);
`
export const FavoriteStart = style.span`
    right: 0;
    color: #ffffff;
    font-size: 50px;
    cursor: pointer;
    padding: 5px 15px;
    position: absolute;
    transition: all .3s;
    text-shadow: 0 0 3px #000000, 0 0 5px #fff284;
    &:hover {
        transform: scale(1.2)
    }
`
export const NotFound = style.p`
    animation: local-name 2s both;
    @keyframes local-name {
    0% { color: transparent; }
    99% { color: transparent; }
    100% { color: awesome; }
    }
`
export const Paper = style.div`
    width: 50vw;
    height: 80vh;
    padding: 15px;
    overflow: auto;
    background: #ffffff;
    border-radius: 10px;
`

export const ModalHeader = style.div`
    display: flex;
    position: fixed;
    background: #ffffffde;
    width: calc(50% - 15px);
    justify-content: space-between;
`

export const CloseModal = style.span`
    padding: 10px;
    color: #808080;
    font-size: 30px;
    cursor: pointer;
`
export const PreviwCoverComicContainer= style.div`
    display: flex;
    align-items: center;
`
export const PreviwCoverComic = style.img`
    max-width: 132px;
    max-height: 200px;
    border-radius: 10px;
    min-height: 200px;
`

export const CharacterNameModal = style.h2`
    padding: 15px 0;
`

export const ComicModalContainer = style.div`
    display: flex;
    margin: 15px 0;
`

export const ComicDescriptionModal = style.div`
    margin: 0 15px;
`