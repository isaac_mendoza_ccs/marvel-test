const hash = 'e05d9f07d3d932f89c06f6f48b76451e';
const apikey = '42b62859a817f26c4c7e22a803fda4be';
const BASE_API = 'http://gateway.marvel.com/v1/public'

function get(url, params = null) {

    let queryString = Object.keys(params).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&');

    let response = fetch(url + queryString, {
        mode: 'cors',
        method: 'GET',
    }).then(function(response) {
        return response.json();
    });
    return response;
}

export function getByCustomUrl(params, url){
    return get( url, {...params, apikey: apikey, hash: hash} )
}

export function getCharacterById(params, id){
    return get( `${BASE_API}/characters/` + id + '?', {...params, apikey: apikey, hash: hash} )
}

export function getCharacters(params) {
    return get( `${BASE_API}/characters?`, {...params, apikey: apikey, hash: hash} )
};

export function getComicByCharacterId(params, characterId) {
    return get( `${BASE_API}/characters/${characterId}/comics?`, {...params, apikey: apikey, hash: hash} )
};