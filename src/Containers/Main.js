import React, {useState, useEffect} from 'react';

import { useStateValue } from '../state';
import Fade from '@material-ui/core/Fade';
import Modal from '@material-ui/core/Modal';
import { useLocation } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import useStyles from '../Components/UseStyles';
import { getByCustomUrl, getCharacterById } from '../endpoints';
import CircularProgress from '@material-ui/core/CircularProgress';

import {
    Paper,
    NotFound,
    CloseModal,
    ModalHeader,
    MainContainer,
    FavoriteStart,
    TargetCharacter,
    PreviwCoverComic,
    LabelCharacterName,
    CharacterNameModal,
    ComicModalContainer,
    ComicDescriptionModal,
    PreviwCoverComicContainer,
} from '../Components/StylesComponents';

const Main = () => {
    
    const classes = useStyles();
    const [{ characterList, favoriteCharacters, ComicsByCharacters, search }, dispatch] = useStateValue();
    const [comicsToShowInModal, setComicsToShowInModal] = useState(null);
    const [modalOpen, setModalOpen] = useState(false);

    const setFavoriteCharacter = (item) => {
        let favoriteCharactersTemp = {...favoriteCharacters}
        if( favoriteCharactersTemp && favoriteCharactersTemp[item.id] ){
            delete favoriteCharactersTemp[item.id]
        }else{
            favoriteCharactersTemp = {...favoriteCharactersTemp, [item.id]: {id: item.id, name: item.name, thumbnail: {path: item.thumbnail.path, extension: item.thumbnail.extension } } }
        }
        dispatch({type: 'favoriteCharacters', value: favoriteCharactersTemp})
    }

    const handleSetComicsToShow = async (character) => {

        setComicsToShowInModal(null);
        let comicsToSearch = null
        let comicsOfCharacter = [];
        let params = null;
        if( Object.keys(ComicsByCharacters).length > 0 && ComicsByCharacters[character.id] ){
            comicsToSearch = ComicsByCharacters[character.id]
        }else{
            if(character.comics){
                comicsToSearch = character.comics.items
            }else{
                params = {...params, ts : 1 }
                let response = await getCharacterById(params, character.id)
                comicsToSearch = response.data.results[0].comics.items
            }
        }

        comicsToSearch.forEach( async (comic) => {
            params = { ts : 1 }
            let response = await getByCustomUrl(params, comic.resourceURI + '?')
            comicsOfCharacter[comicsOfCharacter.length] = response.data.results[0]
        })

        setTimeout( function doSomething() {
            if( comicsToSearch.length !== Object.keys(comicsOfCharacter).length ){
                setTimeout(doSomething, 100);
            }else{
                setComicsToShowInModal( {character: character, comics: comicsOfCharacter } )
            }
        }, 100);
        setModalOpen(true)
    }

    const buildComicsToShowInModal = () => {
        let styleFirst = {marginTop: '110px'}
        return comicsToShowInModal.comics.map( (comic, index) => {
            return  <ComicModalContainer key={index} className="comic-modal-container" style={ index === 0 ? styleFirst : {} }>
                        <PreviwCoverComicContainer>
                            <PreviwCoverComic src={comic.thumbnail.path + '.' + comic.thumbnail.extension}></PreviwCoverComic>
                        </PreviwCoverComicContainer>
                        <ComicDescriptionModal>
                            <h3>{comic.title}</h3>
                            <div>{ ReactHtmlParser(comic.description) }</div>
                        </ComicDescriptionModal>
                    </ComicModalContainer>
        } )
    }

    const showList = (list) => {

        if( list.length > 0 ){
            return list.map( (item, index) => {
                return  <TargetCharacter key={index} onClick={ () => handleSetComicsToShow(item)  }>
                            <FavoriteStart onClick={ (e) => { e.stopPropagation(); setFavoriteCharacter(item)} } >{ item.id in favoriteCharacters ? '★' : '☆' }</FavoriteStart>
                            <img src={item.thumbnail.path + '.' + item.thumbnail.extension} alt={item.name}></img>
                            <LabelCharacterName>{item.name}</LabelCharacterName>
                        </TargetCharacter>
            });
        }else{
            return <NotFound>Character not Found</NotFound>
        }
    }

    const showFavoriteList = (list) => {

        if( Object.keys(list).length > 0 ){
            return Object.keys(list).map( (key) => {

                return  <TargetCharacter key={key} onClick={ () => handleSetComicsToShow(list[key])  }>
                            <FavoriteStart onClick={ (e) => { e.stopPropagation(); setFavoriteCharacter(list[key])} } >{ list[key].id in favoriteCharacters ? '★' : '☆' }</FavoriteStart>
                            <img src={list[key].thumbnail.path + '.' + list[key].thumbnail.extension} alt={list[key].name}></img>
                            <LabelCharacterName>{list[key].name}</LabelCharacterName>
                        </TargetCharacter>
            });
        }else{
            return <NotFound>Character not Found</NotFound>
        }
    }

    useEffect( () => {
        let comicsByCharacter = {}
        if(search && search.length > 0){
            characterList.forEach( async (character) => {
                comicsByCharacter = {...comicsByCharacter, [character.id]: character.comics.items}
            })
            dispatch( {type: 'addComicByCharacter', value: comicsByCharacter } )
        }
    }, [characterList])

    const location = useLocation();

    return (
    <MainContainer>
        { location.pathname === '/favorites' ? showFavoriteList( favoriteCharacters ) : showList( characterList )}
        <Modal
            open={modalOpen}
            closeAfterTransition
            className={classes.modal}
            BackdropProps={{timeout: 500,}}
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            //onClose={handleClose}
            //BackdropComponent={Backdrop}
        >
            <Fade in={modalOpen}>
                <Paper>
                    { !comicsToShowInModal && 
                        <CircularProgress className={classes.circularProgress}/>
                    }
                    { comicsToShowInModal && <>
                        <ModalHeader>
                            <CharacterNameModal>{comicsToShowInModal.character.name}</CharacterNameModal>
                            <CloseModal onClick={ () => setModalOpen(false) }>X</CloseModal>
                        </ModalHeader>
                        {buildComicsToShowInModal()}
                    </>}
                </Paper>
            </Fade>
        </Modal>
    </MainContainer>
    );
};
export default Main;
