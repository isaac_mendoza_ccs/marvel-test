import React, {useEffect, useState} from 'react';

import queryString from 'query-string'
import { useStateValue } from '../state';
import { Redirect } from 'react-router-dom';
import HeaderComponent from '../Components/HeaderComponent';
import { getCharacters, getComicByCharacterId } from '../endpoints';

const Header = () => {

    const [{ search }, dispatch] = useStateValue();
    const [ defaultRoute, setDefaultRoute] = useState(null);

    const handleSetComicsByCharacter = async (characters, comic) => {

        let response = null
        let comicsByCharacter = {}
        let params = null
        let comicFix = comic.split('(')[0]

        characters.forEach( async (character) => {
            params = params = { titleStartsWith: comicFix, ts : 1 }
            response = await getComicByCharacterId(params, character.id)
            comicsByCharacter = {...comicsByCharacter, [character.id]: response.data.results}
        })

        setTimeout( function doSomething() {
            if( characters.length !== Object.keys(comicsByCharacter).length ){
                setTimeout(doSomething, 100);
            }else{
                dispatch( {type: 'addComicByCharacter', value: comicsByCharacter } )
            }
        }, 100);
    }

    const handleGetCharaters = async (params, type, comics) => {

        let response = await getCharacters(params)
        if( comics ){
            if( typeof(comics) === "string" ){
                handleSetComicsByCharacter(response.data.results, comics)
            }else{
                comics.forEach( comic => {
                    handleSetComicsByCharacter(response.data.results, comic)
                })
            }
        }
        dispatch( {type: type, value: response.data.results} )
    }
    
    const handleGetCharatersRandom = async (params) => {

        let response = await getCharacters(params)
        let characterRand = response.data.results[Math.floor( (Math.random() * response.data.count) + 1 )]
        if( characterRand ){
            setDefaultRoute( characterRand.name.split(' ').join('-') )
            localStorage.setItem("lastCharacter", characterRand.name.split(' ').join('-') )
            dispatch({type: 'search', value: characterRand.name})
        }
    }

    useEffect( () => {

        if(search){
            if( localStorage.getItem("lastCharacter") !== search){
                dispatch( {type: 'characterList', value: []} )
            }
            let charactersTemp = search.split(',')
            let params = null;
            charactersTemp.forEach( name => {
                params = { ts : 1, nameStartsWith: name.trim() }
                handleGetCharaters(params, 'addCharacterList')
            });
            localStorage.setItem("lastCharacter", charactersTemp.join('-') )
        }
    }, [search]);

    useEffect( () => {

        let queryStringTemp = queryString.parse(window.location.search)
        let params = { ts : 1 }
        let charactersHref = window.location.href.split('/')

        if( window.location.search === localStorage.getItem("lastCharacter") ){
            handleGetCharatersRandom(params)
        }else{
            if( queryStringTemp.character ){
                if( typeof(queryStringTemp.character) === "string" ){
                    params = {...params, nameStartsWith: queryStringTemp.character }
                    handleGetCharaters(params, 'addCharacterList', queryStringTemp.comic)
                }else{
                    queryStringTemp.character.forEach ( name => {
                        params = {...params, nameStartsWith: name }
                        handleGetCharaters(params, 'addCharacterList', queryStringTemp.comic)
                    });
                }
            }
            setDefaultRoute( charactersHref[(charactersHref.length - 1)] )
            localStorage.setItem("lastCharacter", window.location.search )
        }
    }, []);

    return (<>
    { defaultRoute && <Redirect from="/" to={`/${ defaultRoute }`} /> }
        <HeaderComponent search={search} dispatch={dispatch} /> 
    </>);
};
export default Header;